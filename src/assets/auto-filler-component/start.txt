<template>
  <!-- The final result of the auto-filler generator -->
  <!-- bg-white mt-7 p-4 -->
  <div class="rounded font-medium leading-8">
    <div>
      <div v-if="template && template.fields && template.fields.length > 0">
        <template v-for="(b, i) in template.fields">
          <span :key="i">
            <vue-typed-js
              v-if="b.showBefore"
              @onComplete="stepForward(i, b, 'before')"
              class=""
              v-bind="typedParams"
              :strings="[b.textBefore]"
            >
              <span class="typing"></span>
            </vue-typed-js>
            <!-- Begin Auto-Complete Component -->
            <div class="relative mx-1 inline-block w-auto text-left">
              <button
                :ref="`btn_options_${i}`"
                v-if="b.showInput"
                @click.stop="openOptionsMenu(i, b)"
                :aria-expanded="b.menuOpened ? 'true' : 'false'"
                aria-haspopup="true"
                :class="`
                      w-auto focus:outline-none border-b-2
                      ${
                        (b.allowMultiple &&
                          Array.isArray(b.value) &&
                          b.value.length > 0) ||
                        (!b.allowMultiple && b.value)
                          ? 'font-bold text-gray-900 border-gray-800'
                          : 'font-medium text-gray-500 border-gray-400'
                      }
                    `"
              >
                <span v-if="b.value">
                  {{
                    b.type == "menu" &&
                    b.allowMultiple &&
                    Array.isArray(b.value)
                      ? b.value.length > 0
                        ? b.value.join(", ")
                        : b.placeholder
                      : b.value
                      ? b.value
                      : b.placeholder
                  }}
                </span>
                <vue-typed-js
                  v-else
                  @onComplete="stepForward(i, b, 'input')"
                  class="mr-1"
                  v-bind="typedParams"
                  :strings="[b.placeholder]"
                >
                  <span class="typing"></span>
                </vue-typed-js>
              </button>
              <div
                v-show="b.menuOpened"
                :class="`
                      menu-body z-10 origin-top-left absolute 
                      mt-2 w-80 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none
                    `"
              >
                <div role="none" class="block w-full">
                  <div v-if="['text', 'menu'].includes(b.type)">
                    <div class="px-4 py-3">
                      <input
                        :type="
                          b.fieldType == 'date'
                            ? 'date'
                            : b.fieldType == 'phone'
                            ? 'number'
                            : 'text'
                        "
                        @keypress.enter="
                          b.type == 'text'
                            ? selectOption(b.tempValue, b, i, false)
                            : () => {}
                        "
                        v-model="b.tempValue"
                        class="menu-search transition focus:outline-none block w-full hover:border-gray-400 focus:border-indigo-500 px-2 py-2 border-2 border-gray-200 rounded"
                        placeholder="Type here"
                      />
                    </div>
                    <div
                      v-if="b.type == 'menu'"
                      class="block w-full menu-items-container mb-2"
                      role="menu"
                      aria-orientation="vertical"
                      aria-labelledby="options-menu"
                    >
                      <div class="py-1 block w-full" role="none">
                        <template v-for="(o, oi) in b.menuOptions">
                          <button
                            @click="
                              selectOption(
                                o,
                                b,
                                i,
                                b.allowMultiple ? false : true
                              )
                            "
                            v-if="
                              b.tempValue
                                ? o
                                    .toLowerCase()
                                    .includes(b.tempValue.toLowerCase())
                                : true
                            "
                            :key="oi"
                            :class="`
                              focus:outline-none text-left transition block px-4 font-medium w-full py-2 text-sm 
                              ${
                                (b.allowMultiple &&
                                  Array.isArray(b.value) &&
                                  b.value.includes(o)) ||
                                (!b.allowMultiple && o == b.value)
                                  ? 'text-indigo-600 bg-indigo-100 hover:bg-indigo-200'
                                  : 'text-gray-700 hover:bg-gray-100 hover:text-gray-900'
                              }
                            `"
                            role="menuitem"
                          >
                            {{ o }}
                            <span class="float-right font-bold">
                              <span
                                v-if="
                                  (b.allowMultiple &&
                                    Array.isArray(b.value) &&
                                    b.value.includes(o)) ||
                                  (!b.allowMultiple && o == b.value)
                                "
                                >&check;</span
                              >
                            </span>
                            <div class="clearfix"></div>
                          </button>
                        </template>
                      </div>
                    </div>
                  </div>
                  <div class="mt-1 mb-3 px-4" v-if="b.error">
                    <div
                      class="bg-red-100 border border-red-400 text-red-700 px-3 py-2 rounded relative"
                      role="alert"
                    >
                      <span class="block">
                        {{ b.error }}
                      </span>
                    </div>
                  </div>
                  <div
                    v-if="
                      b.type == 'text' || (b.type == 'menu' && b.allowMultiple)
                    "
                    class="px-4 py-3 bg-gray-100 text-right"
                  >
                    <button
                      @click="
                        selectOption(
                          b.type == 'menu' && b.allowMultiple
                            ? 'tmp'
                            : b.tempValue,
                          b,
                          i,
                          b.type == 'menu' && b.allowMultiple
                        )
                      "
                      class="px-4 py-2 rounded font-medium text-sm text-center focus:outline-none bg-indigo-100 text-indigo-600 hover:bg-indigo-500 hover:text-white transition"
                    >
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <!-- ../End: Auto-Complete Component -->
            <vue-typed-js
              v-if="b.showAfter"
              @onComplete="stepForward(i, b, 'after')"
              class="mr-1"
              v-bind="typedParams"
              :strings="[b.textAfter]"
            >
              <span class="typing"></span>
            </vue-typed-js>
          </span>
        </template>
        <div class="px-3" v-if="template.hasSignature">
          <div class="mt-5 mb-3">
            <hr />
          </div>
          <div class="mb-3">
            <h5 class="text-lg font-medium text-gray-900">Your Signature</h5>
          </div>
          <div
            class="font-medium border-2 border-gray-300 rounded text-gray-600"
            @click="saveSignature()"
          >
            <vue-signature
              :disabled="false"
              w="100%"
              h="300px"
              ref="signature_pad"
              :sigOption="signatureOptions"
            ></vue-signature>
          </div>
          <div class="bg-gray-50 px-4 py-3 flex">
            <div class="flex-grow">
              <!--  -->
            </div>
            <div class="flex-none">
              <button
                type="button"
                @click.stop="clearSignature()"
                class="border border-red-300 px-4 mt-3 sm:mt-0 transition w-full justify-center rounded-md py-2 text-base font-medium hover:text-red-600 text-red-400 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Clear
              </button>
              <button
                type="button"
                @click.stop="undoSignature()"
                class="border border-gray-300 px-4 mt-3 sm:mt-0 transition w-full justify-center rounded-md py-2 text-base font-medium hover:text-gray-800 text-gray-600 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Undo
              </button>
            </div>
            <div class="flex-grow">
              <!--  -->
            </div>
          </div>
        </div>
      </div>
      <div v-else>No fields are created.</div>
    </div>
    <div class="mt-6">
      <div class="flex">
        <div class="flex-grow"></div>
        <div class="flex-none">
          <button
            @click.stop="finish()"
            role="button"
            :disabled="canGoNext ? false : true"
            :class="`
              transform focus:outline-none shadow-sm duration-300 transition-all rounded text-sm font-medium px-6 py-3
              ${
                canGoNext
                  ? 'hover:shadow-lg hover:-translate-y-0.5 hover:bg-indigo-500 hover:text-white bg-indigo-100 text-indigo-600'
                  : 'bg-gray-200 text-gray-400 pointer-events-none'
              }
            `"
          >
            {{ finishText }}
          </button>
        </div>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: "AutoFillerComponent",
  components: {
    VueSignature: () => import("vue-signature"),
  },
  props: {
    finishText: {
      type: String,
      default: "Continue",
    },
    returnFullJson: {
      type: Boolean,
      default: false,
    },
    penColor: {
      type: String,
      default: "#000",
    },
    activateBtnAfterFillAll: {
      type: Boolean,
      default: true,
    },
  },
  data: () => ({
    signatureOptions: {
      penColor: "#000",
    },
    typedParams: {
      class: "inline-block",
      "show-cursor": false,
      "fade-out": true,
      loop: false,
      "type-speed": 30,
    },
    template: 