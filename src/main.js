import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

import VueTypedJs from "vue-typed-js";
Vue.use(VueTypedJs);

// used vue-signature instead
// import VueSignature from "vue-signature-pad";
// Vue.use(VueSignature);

import "@/assets/tailwind.min.css";

let app;

if (!app) {
  app = new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount("#app");
}
