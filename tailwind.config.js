module.exports = {
  purge: {
    enabled: true,
    content: ["./src/**/*.html", "./src/**/*.vue"],
    options: {
      keyframes: true,
    },
  },
  darkMode: false,
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
