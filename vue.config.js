module.exports = {
  pwa: {
    workboxOptions: {
      skipWaiting: true,
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule("raw")
      .test(/\.txt$/)
      .use("raw-loader")
      .loader("raw-loader")
      .end();
  },
};
